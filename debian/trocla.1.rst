======
trocla
======

-----------------
trocla executable
-----------------

:Author: Jérôme Charaoui
:Date: 2023
:Manual section: 1

Synopsis
========

| ``trocla`` (--help | --version)
| ``trocla`` [--config | --trace] SUBCOMMAND [ARGS]

Description
===========

``trocla`` is a password store for password creation and retrieval.

Options
=======

-c, --config
   Path to configuration file.

-h, --help
   Show the help message and exit.

--trace
   Show stack trace on failure.

-V, --version
   Show the version number and exit.

Subcommands
===========

``create`` [`--length` | `--no-random` ] `<key>` `<format>`
  Create a random password (if not already stored in trocla) and store its plain
  text under the specified key. Some formats require additional options.

  Use `--length` to define the length of the generated password.

  Use `--no-random` to skip generating a random password if a plain password is
  not already available for this key.

``get`` `<key>` `<format>`
  Return a stored password (does not create one if absent).

``set`` [`--no-format` | (`-p` | `--password`) `<password>`] `<key>` `<format>`
  Prompt for a plain password and save it under the appropriate key/format.
  Alternatively, the password may be supplied via standard input or using file
  redirection.

  Use `--no-format` to supply an already-formatted password and store it without
  a corresponding plain password.

  Use `--password` to supply the password on the command-line.

``reset`` `<key>` `<format>`
  Recreate a salted shadow-style hash, without creating a new plain text
  password. If a plain password exists for this key, all the previously hashed
  passwords are deleted.

``delete`` `<key>` `[format]`
  Delete the passwords at the specified key and (optionally) format.

``formats``
  List available and supported formats.

``search`` `<string>`
  Search among the password store keys for the specified string (only available
  for Sequel and YAML backends). The string may be a regular expression.

Bugs
====

Bugs with this software may be reported to your distribution's bug tracker or
upstream at https://github.com/duritong/trocla/issues

See also
========

For more information, see /usr/share/doc/trocla/README.md
